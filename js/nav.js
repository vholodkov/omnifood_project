const openMenuClass = 'nav-open';
const htmlElement = document.getElementsByTagName('html')[0];
const navElement = document.getElementsByClassName('header')[0];

const toggleMenu = function () {
	if (navElement) {
		if (navElement.classList.contains(openMenuClass)) {
			hideMenu();
		} else {
			openMenu();
		}
	}
}
function openMenu() {
	htmlElement.style.overflow = "hidden";
	navElement.classList.add(openMenuClass);
}
function hideMenu() {
	htmlElement.style.overflow = "unset";
	htmlElement.style.overflowX = "hidden"
	navElement.classList.remove(openMenuClass);
}
function goToSection(event) {
	const toElement = document.querySelector(event.dataset['to']);
	if(!toElement) {
		throw new Error("Element not found");
	}
	hideMenu();
	setTimeout(() =>{
		toElement.scrollIntoView({behavior: "smooth"});
	}, 1500)
}
